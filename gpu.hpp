#ifndef GPU_HPP_INCLUDED
#define GPU_HPP_INCLUDED

// c standard library headers
#include <cstdio>
#include <cstdlib>

// c++ standard library headers
#include <iostream>

// other library headers
#include <cuda.h>
#include <cuda_runtime.h>

// project headers

// GPU Error detection function
#define GPUErrorCheck(ans) \
  { GPUAssert((ans), __FILE__, __LINE__); }
inline void GPUAssert(cudaError_t code, const char* file, int line,
                      bool abort = true) {
  if (code != cudaSuccess) {
    std::cerr << "GPUassert error: " << cudaGetErrorName(code) << ", "
              << cudaGetErrorString(code) << " in file " << file << " at line "
              << line << "\n";
    if (abort) exit(code);
  }
}

// wrapper for cudaMalloc
template <typename T>
inline cudaError_t cudaAlloc(T*& device_pointer, const size_t elements = 1) {
  return cudaMalloc((void**)&device_pointer, elements * sizeof(T));
}

// STL like vector class for device
template <typename T = double>
class Vector {
 public:
  // constructors
  Vector() = default;  // default constructor
  __host__ __device__ Vector(const size_t size, const T& value = {})
      : array_{new T[size]}, size_{size} {
    for (size_t n = 0; n < size_; n++) array_[n] = value;
  }  // explicit constructor
  __host__ __device__ Vector(const size_t size, const T* array)
      : array_{new T[size]}, size_{size} {
    for (size_t n = 0; n < size_; n++) array_[n] = array[n];
  }  // explicit constructor
  __host__ __device__ Vector(const size_t size, T* array)
      : array_{array}, size_{size} {
    array = nullptr;
  }  // explicit constructor
  __host__ __device__ Vector(const Vector& v)
      : array_{new T[v.size_]}, size_{v.size_} {
    for (size_t n = 0; n < size_; n++) array_[n] = v.array_[n];
  }  // copy constructor
  __host__ __device__ Vector(Vector&& v) noexcept
      : array_{v.array_}, size_{v.size_} {
    v.array_ = nullptr;
    v.size_ = {};
  }  // move constructor

  // assignment operators
  __host__ __device__ Vector& operator=(const Vector& v) {
    if (this not_eq &v) {
      size_ = v.size_;
      delete[] array_, array_ = new T[size_];
      for (size_t n = 0; n < size_; n++) array_[n] = v.array_[n];
    }
    return *this;
  }  // copy assignment
  __host__ __device__ Vector& operator=(Vector&& v) noexcept {
    if (this not_eq &v) {
      size_ = v.size_, v.size_ = {};
      delete[] array_, array_ = v.array_, v.array_ = nullptr;
    }
    return *this;
  }  // move assignment

  __host__ __device__ ~Vector() {
    delete[] array_, array_ = nullptr;
  }  // destructor

  // member functions
  __host__ __device__ void Assign(const size_t count, const T& value) {
    size_ = count;
    delete[] array_, array_ = new T[size_];
    for (size_t n = 0; n < size_; n++) array_[n] = value;
  }

  // member access
  // access specified element with bounds checking
  __host__ __device__ inline T& At(const size_t index) {
    // TODO: think of the exception
    if (index < size_) return array_[index];
  }
  __host__ __device__ inline const T& At(const size_t index) const {
    // TODO: think of the exception
    if (index < size_) return array_[index];
  }

  // access specified element
  __host__ __device__ inline T& operator[](const size_t index) {
    return array_[index];
  }
  __host__ __device__ inline const T& operator[](const size_t index) const {
    return array_[index];
  }

  // access the first element
  __host__ __device__ inline T& Front() { return array_[0]; }
  __host__ __device__ inline const T& Front() const { return array_[0]; }

  // access the last element
  __host__ __device__ inline T& Back() { return array_[size_ - 1]; }
  __host__ __device__ inline const T& Back() const { return array_[size_ - 1]; }

  // direct access to the underlying array
  __host__ __device__ inline T* Data() noexcept { return array_; }
  __host__ __device__ inline const T* Data() const noexcept { return array_; }

  // capacity
  // checks whether the container is empty
  __host__ __device__ inline bool Empty() const noexcept { return size_ == 0; }

  // returns the number of elements
  __host__ __device__ inline size_t Size() const noexcept { return size_; }

  // modifiers
  // clears the contents
  __host__ __device__ inline void Clear() noexcept {
    delete[] array_, array_ = nullptr;
    size_ = 0;
  }

 protected:
  /* data */

 private:
  /* data */
  T* array_{};
  size_t size_{};
};      // class Vector
#endif  //	 GPU_HPP_INCLUDED
