#ifndef SCALAR_FIELD_HPP_INCLUDED
#define SCALAR_FIELD_HPP_INCLUDED

// c standard library headers
#include <cstdio>  // printf

// c++ standard library headers
#include <utility>  // std::move

// other library headers

// project headers
#include "gpu.hpp"

namespace solvers {
// Forward declarations

// Base template parameter has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double>
struct MeshResolution {
  // constructors
  MeshResolution() = default;  // default constructor
  MeshResolution(const T res)
      : Δx{res}, Δy{res}, Δz{res} {}  // explicit constructor
  MeshResolution(const T dx, const T dy, const T dz)
      : Δx{dx}, Δy{dy}, Δz{dz} {}                      // explicit constructor
  MeshResolution(const MeshResolution& mr) = default;  // copy constructor
  MeshResolution(MeshResolution&& mr) noexcept = default;  // move constructor

  MeshResolution& operator=(const MeshResolution& mr) =
      default;  // copy assignment
  MeshResolution& operator=(MeshResolution&& mr) noexcept =
      default;  // move assignment

  ~MeshResolution() = default;  // default destructor

  T Δx{}, Δy{}, Δz{};
};  // Struct MeshResolution

struct MeshSize {
  // constructors
  MeshSize() = default;  // default constructor
  MeshSize(const size_t dim_x, const size_t dim_y, const size_t dim_z)
      : x{dim_x},
        y{dim_y},
        z{dim_z},
        xy{dim_x * dim_y},
        yz{dim_y * dim_z},
        zx{dim_z * dim_x},
        xyz{dim_x * dim_y * dim_z} {}          // explicit constructor
  MeshSize(const MeshSize& md) = default;      // copy constructor
  MeshSize(MeshSize&& md) noexcept = default;  // move constructor

  MeshSize& operator=(const MeshSize& md) = default;      // copy assignment
  MeshSize& operator=(MeshSize&& md) noexcept = default;  // move assignment

  ~MeshSize() = default;  // default destructor

  size_t x{}, y{}, z{};
  size_t xy{}, yz{}, zx{}, xyz{};
};  // struct MeshSize

// Boundary conditions, method of virtual points: ∂f/∂t + bt ft = c
// Base template parameter has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double>
struct BoundaryConditions {
  // constructors
  BoundaryConditions() = default;  // default constructor
  BoundaryConditions(const T bx0, const T cx0, const T bx1, const T cx1,
                     const T by0, const T cy0, const T by1, const T cy1,
                     const T bz0, const T cz0, const T bz1, const T cz1)
      : bx₀{bx0},
        cx₀{cx0},
        bx₁{bx1},
        cx₁{cx1},
        by₀{by0},
        cy₀{cy0},
        by₁{by1},
        cy₁{cy1},
        bz₀{bz0},
        cz₀{cz0},
        bz₁{bz1},
        cz₁{cz1} {}  // explicit constructor
  BoundaryConditions(const BoundaryConditions& bd) =
      default;  // copy constructor
  BoundaryConditions(BoundaryConditions&& bd) noexcept =
      default;  // move constructor

  BoundaryConditions& operator=(const BoundaryConditions& bd) =
      default;  // copy assignment
  BoundaryConditions& operator=(BoundaryConditions&& bd) noexcept =
      default;  // move assignment

  ~BoundaryConditions() = default;  // default destructor

  T bx₀{}, cx₀{}, bx₁{}, cx₁{};
  T by₀{}, cy₀{}, by₁{}, cy₁{};
  T bz₀{}, cz₀{}, bz₁{}, cz₁{};
};  // struct BoundaryConditions

// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T, size_t reaction_count, size_t source_count,
          size_t auxiliary_reaction_count, size_t auxiliary_source_count>
class ScalarField {
  template <typename U>
  using ScalarArray = Vector<U>;

  using SourcePositionArray = Vector<size_t>;

  template <typename U>
  using SourceValueArray = Vector<U>;

 public:
  /* data */
  BoundaryConditions<T> boundary_{};
  MeshSize size_{};
  T diffusivity_{};

  // constructors
  ScalarField() = default;  // default constructor
  __host__ __device__ ScalarField(const BoundaryConditions<T>& bc,
                                  const MeshSize& md, const T D = 0.0,
                                  const T value = 0.0)
      : boundary_{bc},
        size_{md},
        diffusivity_{D},
        scalar_(md.xyz, value),
        source_flux_(source_count),
        source_flux_residual_constant_(source_count),
        source_flux_residual_variable_(source_count),
        source_position_(source_count) {}  // explicit constructor
  __host__ __device__ ScalarField(const BoundaryConditions<T>& bc,
                                  const MeshSize& md, const T D,
                                  const T* scalar, const T* source_flux,
                                  const T* source_flux_residual_constant,
                                  const T* source_flux_residual_variable,
                                  const size_t* source_position)
      : boundary_{bc},
        size_{md},
        diffusivity_{D},
        scalar_(md.xyz, scalar),
        source_flux_(source_count, source_flux),
        source_flux_residual_constant_(source_count,
                                       source_flux_residual_constant),
        source_flux_residual_variable_(source_count,
                                       source_flux_residual_variable),
        source_position_(source_count, source_position) {
  }  // explicit constructor
  __host__ __device__ ScalarField(const BoundaryConditions<T>& bc,
                                  const MeshSize& md, const T D, T* scalar,
                                  T* source_flux,
                                  T* source_flux_residual_constant,
                                  T* source_flux_residual_variable,
                                  size_t* source_position)
      : boundary_{bc},
        size_{md},
        diffusivity_{D},
        scalar_(md.xyz, scalar),
        source_flux_(source_count, source_flux),
        source_flux_residual_constant_(source_count,
                                       source_flux_residual_constant),
        source_flux_residual_variable_(source_count,
                                       source_flux_residual_variable),
        source_position_(source_count, source_position) {
  }  // explicit constructor

  ScalarField(const ScalarField& sf) = default;      // copy constructor
  ScalarField(ScalarField&& sf) noexcept = default;  // move constructor

  // assignment operators
  ScalarField& operator=(const ScalarField& sf) = default;  // copy assignment
  ScalarField& operator=(ScalarField&& sf) noexcept =
      default;  // move assignment

  ~ScalarField() = default;  // default destructor

  __host__ __device__ inline size_t CartesianToLinearIndex(
      const size_t i, const size_t j, const size_t k) const {
    // x is the fastest growing, then y, then z
    if (i >= size_.x || j >= size_.y || k >= size_.z) {
      printf(
          "index / size: %lu / %lu. %lu / %lu, %lu / %lu, file: "
          "%s, line: %d\n",
          i, size_.x, j, size_.y, k, size_.z, __FILE__, __LINE__);
      return size_.xyz;
    }

    return i + size_.x * j + size_.xy * k;
  }
  __host__ __device__ void LinearToCartesianIndex(size_t index, size_t& i,
                                                  size_t& j, size_t& k) {
    // x is the fastest growing, then y, then z
    if (index >= size_.xyz) {
      i = size_.x;
      j = size_.y;
      k = size_.z;
      printf(
          "index / size: %lu / %lu. %lu / %lu, %lu / %lu, file: "
          "%s, line: %d\n",
          i, size_.x, j, size_.y, k, size_.z, __FILE__, __LINE__);
      return;
    }

    k = index / size_.xy;
    index -= k * size_.xy;

    j = index / size_.x;
    i = index - j * size_.x;
  }
  // read-only
  __host__ __device__ inline T operator()(const size_t i, const size_t j,
                                          const size_t k) const {
    return scalar_[CartesianToLinearIndex(i, j, k)];
  }
  __host__ __device__ inline ScalarArray<T> ReadScalar() const {
    return scalar_;
  }
  __host__ __device__ inline SourcePositionArray ReadSource() const {
    return source_position_;
  }
  __host__ __device__ inline SourceValueArray<T>
  ReadSourceFluxResidualConstant() const {
    return source_flux_residual_constant_;
  }
  __host__ __device__ inline SourceValueArray<T>
  ReadSourceFluxResidualVariable() const {
    return source_flux_residual_variable_;
  }
  // read/write
  __host__ __device__ inline T& operator()(const size_t i, const size_t j,
                                           const size_t k) {
    return scalar_[CartesianToLinearIndex(i, j, k)];
  }
  __host__ __device__ inline ScalarArray<T>& operator()() { return scalar_; }
  __host__ __device__ inline ScalarArray<T>& GetScalar() { return scalar_; }
  __host__ __device__ inline SourcePositionArray& GetSource() {
    return source_position_;
  }
  __host__ __device__ inline SourceValueArray<T>& GetSourceFlux() {
    return source_flux_;
  }
  __host__ __device__ inline SourceValueArray<T>&
  GetSourceFluxResidualConstant() {
    return source_flux_residual_constant_;
  }
  __host__ __device__ inline SourceValueArray<T>&
  GetSourceFluxResidualVariable() {
    return source_flux_residual_variable_;
  }

  __host__ __device__ inline void SetScalar(const ScalarArray<T> scalar) {
    scalar_ = scalar;
  }

  // write source array
  __host__ __device__ inline void SetSourcePosition(
      const SourcePositionArray& source_position) {
    source_position_ = source_position;
  }
  // write source function array
  __host__ __device__ inline void SetSourceFlux(
      const SourceValueArray<T>& flux,
      const SourceValueArray<T>& flux_residual_constant,
      const SourceValueArray<T>& flux_residual_variable) {
    source_flux_ = flux;
    source_flux_residual_constant_ = flux_residual_constant;
    source_flux_residual_variable_ = flux_residual_variable;
  }
  __host__ __device__ inline void SetSources(
      const SourcePositionArray& source_position,
      const SourceValueArray<T>& flux,
      const SourceValueArray<T>& flux_residual_constant,
      const SourceValueArray<T>& flux_residual_variable) {
    source_position_ = source_position;
    source_flux_ = flux;
    source_flux_residual_constant_ = flux_residual_constant;
    source_flux_residual_variable_ = flux_residual_variable;
  }

  __host__ __device__ virtual T HeterogeneityReaction(
      const size_t /* i */, const size_t /* j */, const size_t /* k */,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& /* sf */) const {
    return 0.0;
  }
  __host__ __device__ virtual T HeterogeneityReaction(
      const size_t /* i */, const size_t /* j */, const size_t /* k */) const {
    return 0.0;
  }
  __host__ __device__ virtual T HeterogeneityReactionResidualConstant(
      const size_t /* i */, const size_t /* j */, const size_t /* k */,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& /* sf */) const {
    return 0.0;
  }
  __host__ __device__ virtual T HeterogeneityReactionResidualConstant(
      const size_t /* i */, const size_t /* j */, const size_t /* k */) const {
    return 0.0;
  }
  __host__ __device__ virtual T HeterogeneityReactionResidualVariable(
      const size_t /* i */, const size_t /* j */, const size_t /* k */,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& /* sf */) const {
    return 0.0;
  }
  __host__ __device__ virtual T HeterogeneityReactionResidualVariable(
      const size_t /* i */, const size_t /* j */, const size_t /* k */) const {
    return 0.0;
  }

  // Returns index of the source if found, else the number of
  // sources i.e. source_count
  __host__ __device__ size_t FindSourceIndex(const size_t i, const size_t j,
                                             const size_t k) const {
    if constexpr (source_count) {
      size_t linear_index = CartesianToLinearIndex(i, j, k);

      // Check if linear_index is in the domain of sources
      if (linear_index < source_position_[0] ||
          linear_index > source_position_[source_count - 1])
        return source_count;

      // Binary search when linear_index is in the domain of sources
      size_t left_index = 0, right_index = source_count - 1, middle_index;
      while (left_index <= right_index) {
        middle_index = left_index + (right_index - left_index) / 2;

        // Check if linear_index is present at middle
        if (source_position_[middle_index] == linear_index) return middle_index;

        // Check if linear_index is greater, ignore left half
        if (source_position_[middle_index] < linear_index)
          left_index = middle_index + 1;

        // Check if linear_index is smaller, ignore right half
        else
          right_index = middle_index - 1;
      }

      // if we reach here, then element was not present
      return source_count;
    } else
      return source_count;
  }
  __host__ __device__ inline T HeterogeneitySource(const size_t i,
                                                   const size_t j,
                                                   const size_t k) const {
    if constexpr (source_count > 0) {
      size_t source_index = FindSourceIndex(i, j, k);

      if (source_index == source_count) return 0.0;

      return source_flux_[source_index];
    } else
      return 0.0;
  }
  __host__ __device__ inline T HeterogeneitySourceResidualConstant(
      const size_t i, const size_t j, const size_t k) const {
    if constexpr (source_count > 0) {
      size_t source_index = FindSourceIndex(i, j, k);

      if (source_index == source_count) return 0.0;

      return source_flux_residual_constant_[source_index];
    } else
      return 0.0;
  }
  __host__ __device__ inline T HeterogeneitySourceResidualVariable(
      const size_t i, const size_t j, const size_t k) const {
    if constexpr (source_count > 0) {
      size_t source_index = FindSourceIndex(i, j, k);

      if (source_index == source_count) return 0.0;

      return source_flux_residual_variable_[source_index];
    } else
      return 0.0;
  }

  __host__ friend void CopyScalarFieldFromHostToDevice(
      ScalarField** device_sf_dest, ScalarField& host_sf_source) {
    // Free memory allocated on device for ScalarField
    ScalarFieldDeleteKernel<<<1, 1>>>(device_sf_dest);

    T diffusivity = host_sf_source.diffusivity_;
    T* scalar;
    T *source_flux, *source_flux_residual_constant,
        *source_flux_residual_variable;
    size_t* source;

    // Allocate memory
    GPUErrorCheck(cudaAlloc(scalar, host_sf_source.size_.xyz));
    GPUErrorCheck(cudaAlloc(source_flux, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_constant, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_variable, source_count));
    GPUErrorCheck(cudaAlloc(source, source_count));

    // Copy to device
    GPUErrorCheck(cudaMemcpy(scalar, host_sf_source.GetScalar().Data(),
                             host_sf_source.size_.xyz * sizeof(T),
                             cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source_flux, host_sf_source.GetSourceFlux().Data(),
                             source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_constant,
                   host_sf_source.GetSourceFluxResidualConstant().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_variable,
                   host_sf_source.GetSourceFluxResidualVariable().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source, host_sf_source.GetSource().Data(),
                             source_count * sizeof(size_t),
                             cudaMemcpyHostToDevice));

    ScalarFieldSetupKernel<<<1, 1>>>(device_sf_dest, host_sf_source.boundary_,
                                     host_sf_source.size_, diffusivity, scalar,
                                     source_flux, source_flux_residual_constant,
                                     source_flux_residual_variable, source);

    // Free memory
    GPUErrorCheck(cudaFree(scalar));
    GPUErrorCheck(cudaFree(source_flux));
    GPUErrorCheck(cudaFree(source_flux_residual_constant));
    GPUErrorCheck(cudaFree(source_flux_residual_variable));
    GPUErrorCheck(cudaFree(source));
  }

  __host__ friend void CopyScalarFromDeviceToHost(
      T* host_scalar, ScalarField* const* d_sf_source, const size_t N) {
    T* d_scalar;
    GPUErrorCheck(cudaAlloc(d_scalar, N));

    CopyScalarFromDeviceToHostKernel<<<1, 1>>>(d_scalar, d_sf_source, N);

    GPUErrorCheck(cudaMemcpy(host_scalar, d_scalar, N * sizeof(T),
                             cudaMemcpyDeviceToHost));
    GPUErrorCheck(cudaFree(d_scalar));
  }

 protected:
  /* data */
  ScalarArray<T> scalar_{};
  SourceValueArray<T> source_flux_{}, source_flux_residual_constant_{},
      source_flux_residual_variable_{};
  SourcePositionArray source_position_{};

 private:
  /* data */
};  // class ScalarField

// Buffers at least one ion, reaction_count ≥ 1, auxiliary_reaction_count ≥ 1
// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T, size_t reaction_count, size_t source_count,
          size_t auxiliary_reaction_count, size_t auxiliary_source_count>
class Buffer
    : public ScalarField<T, reaction_count, source_count,
                         auxiliary_reaction_count, auxiliary_source_count> {
 public:
  // constructors
  Buffer() = default;  // default constructor
  __host__ __device__ Buffer(const T k₊ = 0.0, const T k₋ = 0.0,
                             const T b_total = 0.0)
      : k₊_{k₊}, k₋_{k₋}, b_total_{b_total} {}  // explicit constructor
  __host__ __device__ Buffer(const BoundaryConditions<T>& bc,
                             const MeshSize& md, const T D = 0.0,
                             const T value = 0.0, const T k₊ = 0.0,
                             const T k₋ = 0.0, const T b_total = 0.0)
      : ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>(bc, md, D, value),
        k₊_{k₊},
        k₋_{k₋},
        b_total_{b_total} {}  // explicit constructor
  __host__ __device__ Buffer(const BoundaryConditions<T>& bc,
                             const MeshSize& md, const T D, const T*& scalar,
                             const T*& source_flux,
                             const T*& source_flux_residual_constant,
                             const T*& source_flux_residual_variable,
                             const size_t*& source_position, const T k₊ = 0.0,
                             const T k₋ = 0.0, const T b_total = 0.0)
      : ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>(
            bc, md, D, scalar, source_flux, source_flux_residual_constant,
            source_flux_residual_variable, source_position),
        k₊_{k₊},
        k₋_{k₋},
        b_total_{b_total} {}  // explicit constructor
  __host__ __device__ Buffer(const BoundaryConditions<T>& bc,
                             const MeshSize& md, const T D, T* scalar,
                             T* source_flux, T* source_flux_residual_constant,
                             T* source_flux_residual_variable,
                             size_t* source_position, const T k₊ = 0.0,
                             const T k₋ = 0.0, const T b_total = 0.0) noexcept
      : ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>(
            bc, md, D, scalar, source_flux, source_flux_residual_constant,
            source_flux_residual_variable, source_position),
        k₊_{k₊},
        k₋_{k₋},
        b_total_{b_total} {}  // explicit constructor

  Buffer(const Buffer& b) = default;      // copy constructor
  Buffer(Buffer&& b) noexcept = default;  // move constructor

  Buffer& operator=(const Buffer& b) = default;      // copy assignment
  Buffer& operator=(Buffer&& b) noexcept = default;  // move assignment

  ~Buffer() = default;  // default destructor

  // read-only
  __host__ __device__ inline T ReadkPlus() const { return k₊_; }
  __host__ __device__ inline T ReadkMinus() const { return k₋_; }
  __host__ __device__ inline T ReadbTotal() const { return b_total_; }
  // read/write
  __host__ __device__ inline T& GetkPlus() { return k₊_; }
  __host__ __device__ inline T& GetkMinus() { return k₋_; }
  __host__ __device__ inline T& GetbTotal() { return b_total_; }
  __host__ __device__ inline void SetkPlus(const T k₊) { k₊_ = k₊; }
  __host__ __device__ inline void SetkMinus(const T k₋) { k₋_ = k₋; }
  __host__ __device__ inline void SetbTotal(const T b_total) {
    b_total_ = b_total;
  }

  // Rate of buffering
  using ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>::HeterogeneityReaction;

  // Ion has at least one buffer
  __host__ __device__ inline T HeterogeneityReaction(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& ion) const override {
    return k₊_ * ion(i, j, k) * (b_total_ - (*this)(i, j, k)) -
           k₋_ * (*this)(i, j, k);
  }
  // Ion has at least one buffer
  // template <size_t r_count = 1, size_t s_count = 0>
  __host__ __device__ inline T HeterogeneityReactionResidualConstant(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& ion) const override {
    return k₊_ * ion(i, j, k) * b_total_;
  }
  // This is for the respective ion being buffered
  __host__ __device__ inline T HeterogeneityReactionResidualConstant(
      const size_t i, const size_t j, const size_t k) const override {
    return k₋_ * (*this)(i, j, k);
  }
  // Ion has at least one buffer
  // template <size_t r_count = 1, size_t s_count = 0>
  __host__ __device__ inline T HeterogeneityReactionResidualVariable(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& ion) const override {
    return -k₊_ * ion(i, j, k) - k₋_;
  }
  // This is for the respective ion being buffered
  __host__ __device__ inline T HeterogeneityReactionResidualVariable(
      const size_t i, const size_t j, const size_t k) const override {
    return -k₊_ * (b_total_ - (*this)(i, j, k));
  }

  __host__ friend void CopyBufferFromHostToDevice(Buffer** device_buf_dest,
                                                  Buffer& host_buf_source) {
    // Free memory allocated on device for Buffer
    BufferDeleteKernel<<<1, 1>>>(device_buf_dest);

    T diffusivity = host_buf_source.diffusivity_;
    T* scalar;
    T *source_flux, *source_flux_residual_constant,
        *source_flux_residual_variable;
    size_t* source;
    T k₊ = host_buf_source.ReadkPlus();
    T k₋ = host_buf_source.ReadkMinus();
    T b_total = host_buf_source.ReadbTotal();

    // Allocate memory
    GPUErrorCheck(cudaAlloc(scalar, host_buf_source.size_.xyz));
    GPUErrorCheck(cudaAlloc(source_flux, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_constant, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_variable, source_count));
    GPUErrorCheck(cudaAlloc(source, source_count));

    // Copy to device
    GPUErrorCheck(cudaMemcpy(scalar, host_buf_source.GetScalar().Data(),
                             host_buf_source.size_.xyz * sizeof(T),
                             cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source_flux,
                             host_buf_source.GetSourceFlux().Data(),
                             source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_constant,
                   host_buf_source.GetSourceFluxResidualConstant().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_variable,
                   host_buf_source.GetSourceFluxResidualVariable().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source, host_buf_source.GetSource().Data(),
                             source_count * sizeof(size_t),
                             cudaMemcpyHostToDevice));

    BufferSetupKernel<<<1, 1>>>(
        device_buf_dest, host_buf_source.boundary_, host_buf_source.size_,
        diffusivity, scalar, source_flux, source_flux_residual_constant,
        source_flux_residual_variable, source, k₊, k₋, b_total);

    // Free memory
    GPUErrorCheck(cudaFree(scalar));
    GPUErrorCheck(cudaFree(source_flux));
    GPUErrorCheck(cudaFree(source_flux_residual_constant));
    GPUErrorCheck(cudaFree(source_flux_residual_variable));
    GPUErrorCheck(cudaFree(source));
  }

  __host__ friend void CopyBufferFromHostToDevice(
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>** device_buf_dest,
      Buffer& host_buf_source) {
    // Free memory allocated on device for Buffer
    BufferDeleteKernel<<<1, 1>>>(device_buf_dest);

    T diffusivity = host_buf_source.diffusivity_;
    T* scalar;
    T *source_flux, *source_flux_residual_constant,
        *source_flux_residual_variable;
    size_t* source;
    T k₊ = host_buf_source.ReadkPlus();
    T k₋ = host_buf_source.ReadkMinus();
    T b_total = host_buf_source.ReadbTotal();

    // Allocate memory
    GPUErrorCheck(cudaAlloc(scalar, host_buf_source.size_.xyz));
    GPUErrorCheck(cudaAlloc(source_flux, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_constant, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_variable, source_count));
    GPUErrorCheck(cudaAlloc(source, source_count));

    // Copy to device
    GPUErrorCheck(cudaMemcpy(scalar, host_buf_source.GetScalar().Data(),
                             host_buf_source.size_.xyz * sizeof(T),
                             cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source_flux,
                             host_buf_source.GetSourceFlux().Data(),
                             source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_constant,
                   host_buf_source.GetSourceFluxResidualConstant().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_variable,
                   host_buf_source.GetSourceFluxResidualVariable().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source, host_buf_source.GetSource().Data(),
                             source_count * sizeof(size_t),
                             cudaMemcpyHostToDevice));

    BufferSetupKernel<<<1, 1>>>(
        device_buf_dest, host_buf_source.boundary_, host_buf_source.size_,
        diffusivity, scalar, source_flux, source_flux_residual_constant,
        source_flux_residual_variable, source, k₊, k₋, b_total);

    // Free memory
    GPUErrorCheck(cudaFree(scalar));
    GPUErrorCheck(cudaFree(source_flux));
    GPUErrorCheck(cudaFree(source_flux_residual_constant));
    GPUErrorCheck(cudaFree(source_flux_residual_variable));
    GPUErrorCheck(cudaFree(source));
  }

 protected:
  /* data */

 private:
  T k₊_{};       // on/association rate, (concentration χ time)⁻¹
  T k₋_{};       // off/dissociation rate, (time⁻¹)
  T b_total_{};  // total buffer concentration, (concentration)
};               // class Buffer

// Ions may have no buffer, reaction_count ≥ 0
// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T, size_t reaction_count, size_t source_count,
          size_t auxiliary_reaction_count, size_t auxiliary_source_count>
class Ion
    : public ScalarField<T, reaction_count, source_count,
                         auxiliary_reaction_count, auxiliary_source_count> {
 public:
  // constructors
  Ion() = default;  // default constructor
  __host__ __device__ Ion(const BoundaryConditions<T>& bc, const MeshSize& md,
                          const T D = 0.0, const T value = 0.0)
      : ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>(bc, md, D, value) {
  }  // explicit constructor
  __host__ __device__ Ion(const BoundaryConditions<T>& bc, const MeshSize& md,
                          const T D, const T*& scalar, const T*& source_flux,
                          const T*& source_flux_residual_constant,
                          const T*& source_flux_residual_variable,
                          const size_t*& source_position)
      : ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>(
            bc, md, D, scalar, source_flux, source_flux_residual_constant,
            source_flux_residual_variable, source_position) {
  }  // explicit constructor
  __host__ __device__ Ion(const BoundaryConditions<T>& bc, const MeshSize& md,
                          const T D, T* scalar, T* source_flux,
                          T* source_flux_residual_constant,
                          T* source_flux_residual_variable,
                          size_t* source_position) noexcept
      : ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>(
            bc, md, D, scalar, source_flux, source_flux_residual_constant,
            source_flux_residual_variable, source_position) {
  }                                   // explicit constructor
  Ion(const Ion& ion) = default;      // copy constructor
  Ion(Ion&& ion) noexcept = default;  // move constructor

  Ion& operator=(const Ion& ion) = default;      // copy assignment
  Ion& operator=(Ion&& ion) noexcept = default;  // move assignment

  ~Ion() = default;  // default destructor

  // Rate of buffering and sources
  using ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                    auxiliary_source_count>::HeterogeneityReaction;
  using ScalarField<
      T, reaction_count, source_count, auxiliary_reaction_count,
      auxiliary_source_count>::HeterogeneityReactionResidualConstant;
  using ScalarField<
      T, reaction_count, source_count, auxiliary_reaction_count,
      auxiliary_source_count>::HeterogeneityReactionResidualVariable;

  // Buffer this ion
  __host__ __device__ inline T HeterogeneityReaction(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& buffer) const override {
    // negative of buffer rate reaction
    return -buffer.HeterogeneityReaction(i, j, k, *this);
  }
  // Buffer this ion
  __host__ __device__ inline T HeterogeneityReactionResidualConstant(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& buffer) const override {
    return buffer.HeterogeneityReactionResidualConstant(i, j, k);
  }
  // Buffers this ion
  __host__ __device__ inline T HeterogeneityReactionResidualVariable(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, auxiliary_reaction_count, auxiliary_source_count,
                        reaction_count, source_count>& buffer) const override {
    return buffer.HeterogeneityReactionResidualVariable(i, j, k);
  }

  __host__ friend void CopyIonFromHostToDevice(Ion** device_ion_dest,
                                               Ion& host_ion_source) {
    // Free memory allocated on device for Ion
    IonDeleteKernel<<<1, 1>>>(device_ion_dest);

    T diffusivity = host_ion_source.diffusivity_;
    T* scalar;
    T *source_flux, *source_flux_residual_constant,
        *source_flux_residual_variable;
    size_t* source;

    // Allocate memory
    GPUErrorCheck(cudaAlloc(scalar, host_ion_source.size_.xyz));
    GPUErrorCheck(cudaAlloc(source_flux, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_constant, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_variable, source_count));
    GPUErrorCheck(cudaAlloc(source, source_count));

    // Copy to device
    GPUErrorCheck(cudaMemcpy(scalar, host_ion_source.GetScalar().Data(),
                             host_ion_source.size_.xyz * sizeof(T),
                             cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source_flux,
                             host_ion_source.GetSourceFlux().Data(),
                             source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_constant,
                   host_ion_source.GetSourceFluxResidualConstant().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_variable,
                   host_ion_source.GetSourceFluxResidualVariable().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source, host_ion_source.GetSource().Data(),
                             source_count * sizeof(size_t),
                             cudaMemcpyHostToDevice));

    IonSetupKernel<<<1, 1>>>(device_ion_dest, host_ion_source.boundary_,
                             host_ion_source.size_, diffusivity, scalar,
                             source_flux, source_flux_residual_constant,
                             source_flux_residual_variable, source);

    // Free memory
    GPUErrorCheck(cudaFree(scalar));
    GPUErrorCheck(cudaFree(source_flux));
    GPUErrorCheck(cudaFree(source_flux_residual_constant));
    GPUErrorCheck(cudaFree(source_flux_residual_variable));
    GPUErrorCheck(cudaFree(source));
  }

  __host__ friend void CopyIonFromHostToDevice(
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>** device_ion_dest,
      Ion& host_ion_source) {
    // Free memory allocated on device for Ion
    IonDeleteKernel<<<1, 1>>>(device_ion_dest);

    T diffusivity = host_ion_source.diffusivity_;
    T* scalar;
    T *source_flux, *source_flux_residual_constant,
        *source_flux_residual_variable;
    size_t* source;

    // Allocate memory
    GPUErrorCheck(cudaAlloc(scalar, host_ion_source.size_.xyz));
    GPUErrorCheck(cudaAlloc(source_flux, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_constant, source_count));
    GPUErrorCheck(cudaAlloc(source_flux_residual_variable, source_count));
    GPUErrorCheck(cudaAlloc(source, source_count));

    // Copy to device
    GPUErrorCheck(cudaMemcpy(scalar, host_ion_source.GetScalar().Data(),
                             host_ion_source.size_.xyz * sizeof(T),
                             cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source_flux,
                             host_ion_source.GetSourceFlux().Data(),
                             source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_constant,
                   host_ion_source.GetSourceFluxResidualConstant().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(
        cudaMemcpy(source_flux_residual_variable,
                   host_ion_source.GetSourceFluxResidualVariable().Data(),
                   source_count * sizeof(T), cudaMemcpyHostToDevice));
    GPUErrorCheck(cudaMemcpy(source, host_ion_source.GetSource().Data(),
                             source_count * sizeof(size_t),
                             cudaMemcpyHostToDevice));

    IonSetupKernel<<<1, 1>>>(device_ion_dest, host_ion_source.boundary_,
                             host_ion_source.size_, diffusivity, scalar,
                             source_flux, source_flux_residual_constant,
                             source_flux_residual_variable, source);

    // Free memory
    GPUErrorCheck(cudaFree(scalar));
    GPUErrorCheck(cudaFree(source_flux));
    GPUErrorCheck(cudaFree(source_flux_residual_constant));
    GPUErrorCheck(cudaFree(source_flux_residual_variable));
    GPUErrorCheck(cudaFree(source));
  }

 protected:
  /* data */

 private:
  /* data */
};  // class Ion

// setup CUDA kernels
// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0>
__global__ void ScalarFieldSetupKernel(
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** d_sf,
    const BoundaryConditions<T> d_bc, const MeshSize d_md, const T D,
    T* d_scalar, T* d_source_flux, T* d_source_flux_residual_constant,
    T* d_source_flux_residual_variable, size_t* d_source) {
  (*d_sf) = new ScalarField<T, reaction_count, source_count,
                            auxiliary_reaction_count, auxiliary_source_count>(
      d_bc, d_md, D, d_scalar, d_source_flux, d_source_flux_residual_constant,
      d_source_flux_residual_variable, d_source);
}

// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0>
__global__ void ScalarFieldDeleteKernel(
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** sf) {
  delete (*sf);
}

// Buffers at least one ion, reaction_count ≥ 1, auxiliary_reaction_count ≥ 1
// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 1,
          size_t source_count = 0, size_t auxiliary_reaction_count = 1,
          size_t auxiliary_source_count = 0>
__global__ void BufferSetupKernel(
    Buffer<T, reaction_count, source_count, auxiliary_reaction_count,
           auxiliary_source_count>** d_buf,
    const BoundaryConditions<T> d_bc, const MeshSize d_md, const T D,
    T* d_scalar, T* d_source_flux, T* d_source_flux_residual_constant,
    T* d_source_flux_residual_variable, size_t* d_source, const T k₊,
    const T k₋, const T b_total) {
  (*d_buf) = new Buffer<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>(
      d_bc, d_md, D, d_scalar, d_source_flux, d_source_flux_residual_constant,
      d_source_flux_residual_variable, d_source, k₊, k₋, b_total);
}

// Buffers at least one ion, reaction_count ≥ 1, auxiliary_reaction_count ≥ 1
// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 1,
          size_t source_count = 0, size_t auxiliary_reaction_count = 1,
          size_t auxiliary_source_count = 0>
__global__ void BufferSetupKernel(
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** d_buf,
    const BoundaryConditions<T> d_bc, const MeshSize d_md, const T D,
    T* d_scalar, T* d_source_flux, T* d_source_flux_residual_constant,
    T* d_source_flux_residual_variable, size_t* d_source, const T k₊,
    const T k₋, const T b_total) {
  (*d_buf) = new Buffer<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>(
      d_bc, d_md, D, d_scalar, d_source_flux, d_source_flux_residual_constant,
      d_source_flux_residual_variable, d_source, k₊, k₋, b_total);
}

// Buffers at least one ion, reaction_count ≥ 1, auxiliary_reaction_count ≥ 1
// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 1,
          size_t source_count = 0, size_t auxiliary_reaction_count = 1,
          size_t auxiliary_source_count = 0>
__global__ void BufferDeleteKernel(
    Buffer<T, reaction_count, source_count, auxiliary_reaction_count,
           auxiliary_source_count>** buf) {
  delete (*buf);
}

// Buffers at least one ion, reaction_count ≥ 1, auxiliary_reaction_count ≥ 1
// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 1,
          size_t source_count = 0, size_t auxiliary_reaction_count = 1,
          size_t auxiliary_source_count = 0>
__global__ void BufferDeleteKernel(
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** buf) {
  delete ((Buffer<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>*)(*buf));
}

// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0>
__global__ void IonSetupKernel(
    Ion<T, reaction_count, source_count, auxiliary_reaction_count,
        auxiliary_source_count>** d_ion,
    const BoundaryConditions<T> d_bc, const MeshSize d_md, const T D,
    T* d_scalar, T* d_source_flux, T* d_source_flux_residual_constant,
    T* d_source_flux_residual_variable, size_t* d_source) {
  (*d_ion) = new Ion<T, reaction_count, source_count, auxiliary_reaction_count,
                     auxiliary_source_count>(
      d_bc, d_md, D, d_scalar, d_source_flux, d_source_flux_residual_constant,
      d_source_flux_residual_variable, d_source);
}

// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0>
__global__ void IonSetupKernel(
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** d_ion,
    const BoundaryConditions<T> d_bc, const MeshSize d_md, const T D,
    T* d_scalar, T* d_source_flux, T* d_source_flux_residual_constant,
    T* d_source_flux_residual_variable, size_t* d_source) {
  (*d_ion) = new Ion<T, reaction_count, source_count, auxiliary_reaction_count,
                     auxiliary_source_count>(
      d_bc, d_md, D, d_scalar, d_source_flux, d_source_flux_residual_constant,
      d_source_flux_residual_variable, d_source);
}

// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0>
__global__ void IonDeleteKernel(
    Ion<T, reaction_count, source_count, auxiliary_reaction_count,
        auxiliary_source_count>** ion) {
  delete (*ion);
}

// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0>
__global__ void IonDeleteKernel(
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** ion) {
  delete ((Ion<T, reaction_count, source_count, auxiliary_reaction_count,
               auxiliary_source_count>*)(*ion));
}

// Base template parameter, T, has to be one of float, double or long double,
// otherwise the behavior is undefined
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0>
__global__ void CopyScalarFromDeviceToHostKernel(
    T* d_scalar_dest,
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>* const* d_sf_source,
    const size_t N) {
  if (N not_eq (*d_sf_source)->size_.xyz) asm("trap;");

  for (size_t i = 0; i < N; i++)
    d_scalar_dest[i] = (*d_sf_source)->ReadScalar()[i];
}
}  // namespace solvers
#endif  //	 SCALAR_FIELD_HPP_INCLUDED
