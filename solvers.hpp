#ifndef SOLVERS_HPP_INCLUDED
#define SOLVERS_HPP_INCLUDED

// c standard library headers
#include <cstdio>

// c++ standard library headers
#include <utility>  // std::move

// other library headers

// project headers
#include "gpu.hpp"
#include "scalar_field.hpp"

namespace solvers {
// Abstract base class
template <typename T, size_t reaction_count, size_t source_count,
          size_t auxiliary_reaction_count, size_t auxiliary_source_count,
          typename... Fields>
class Solver3D {
 public:
  // constructors
  Solver3D() = default;  // default constructor
  __host__ __device__ Solver3D(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const MeshResolution<T>& mr, const T Δt = 0.0)
      : resolution_{mr}, size_{sf.size_}, Δt_{Δt} {}  // explicit constructor
  Solver3D(const Solver3D& s) = default;              // copy constructor
  Solver3D(Solver3D&& s) noexcept = default;          // move constructor

  // assignment operators
  Solver3D& operator=(const Solver3D& s) = default;      // copy assignment
  Solver3D& operator=(Solver3D&& s) noexcept = default;  // move assignment

  ~Solver3D() = default;  // default destructor

  __host__ __device__ virtual void Initialize(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>&
          sf) = 0;
  __host__ __device__ virtual void TimeStep(
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>& sf,
      Fields&... fs) = 0;

 protected:
  /* data */
  MeshResolution<T> resolution_{};
  MeshSize size_{};
  T Δt_{};

  /* methods */
  // o(h²) finite-difference for second-derivative approximations
  // (∂²f/∂x²) Δx² ≈ f_(i-1) - 2f_(i) + f_(i+1)
  __host__ __device__ virtual inline T Operationδ²x(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const size_t i, const size_t j, const size_t k) {
    // check if diffusion is restricted
    if (sf.size_.x == 1) return 0.0;

    // Boundary conditions, method of virtual points
    if (i == 0)
      return sf.boundary_.bx₀ == 0 && sf.boundary_.cx₀ == 0  // no flux
                 ? sf(1, j, k) - sf(0, j, k)
                 : 2.0 * ((resolution_.Δx * sf.boundary_.bx₀ - 1.0) *
                              sf(0, j, k) +
                          sf(1, j, k) - resolution_.Δx * sf.boundary_.cx₀);
    if (i == sf.size_.x - 1)
      return sf.boundary_.bx₁ == 0 && sf.boundary_.cx₁ == 0  //  no flux
                 ? sf(sf.size_.x - 2, j, k) - sf(sf.size_.x - 1, j, k)
                 : 2.0 * (sf(sf.size_.x - 2, j, k) +
                          (-resolution_.Δx * sf.boundary_.bx₁ - 1.0) *
                              sf(sf.size_.x - 1, j, k) +
                          resolution_.Δx * sf.boundary_.cx₁);

    return sf(i - 1, j, k) - 2.0 * sf(i, j, k) + sf(i + 1, j, k);
  }
  // (∂²f/∂y²) Δy² ≈ f_(j-1) - 2f_(j) + f_(j+1)
  __host__ __device__ virtual inline T Operationδ²y(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const size_t i, const size_t j, const size_t k) {
    // check if diffusion is restricted
    if (sf.size_.y == 1) return 0.0;

    // Boundary conditions, method of virtual points
    if (j == 0)
      return sf.boundary_.by₀ == 0 && sf.boundary_.cy₀ == 0  // no flux
                 ? sf(i, 1, k) - sf(i, 0, k)
                 : 2.0 * ((resolution_.Δy * sf.boundary_.by₀ - 1.0) *
                              sf(i, 0, k) +
                          sf(i, 1, k) - resolution_.Δy * sf.boundary_.cy₀);
    if (j == sf.size_.y - 1)
      return sf.boundary_.by₁ == 0 && sf.boundary_.cy₁ == 0  // no flux
                 ? sf(i, sf.size_.y - 2, k) - sf(i, sf.size_.y - 1, k)
                 : 2.0 * (sf(i, sf.size_.y - 2, k) +
                          (-resolution_.Δy * sf.boundary_.by₁ - 1.0) *
                              sf(i, sf.size_.y - 1, k) +
                          resolution_.Δy * sf.boundary_.cy₁);

    return sf(i, j - 1, k) - 2.0 * sf(i, j, k) + sf(i, j + 1, k);
  }
  // (∂²f/∂z²) Δz²≈ f_(k-1) - 2f_(k) + f_(k+1)
  __host__ __device__ virtual inline T Operationδ²z(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const size_t i, const size_t j, const size_t k) {
    // check if diffusion is restricted
    if (sf.size_.z == 1) return 0.0;

    // Boundary conditions, method of virtual points
    if (k == 0)
      return sf.boundary_.bz₀ == 0 && sf.boundary_.cz₀ == 0  // no flux
                 ? sf(i, j, 1) - sf(i, j, 0)
                 : 2.0 * ((resolution_.Δz * sf.boundary_.bz₀ - 1.0) *
                              sf(i, j, 0) +
                          sf(i, j, 1) - resolution_.Δz * sf.boundary_.cz₀);
    if (k == sf.size_.z - 1)
      return sf.boundary_.bz₁ == 0 && sf.boundary_.cz₁ == 0  // no flux
                 ? sf(i, j, sf.size_.z - 2) - sf(i, j, sf.size_.z - 1)
                 : 2.0 * (sf(i, j, sf.size_.z - 2) +
                          (-resolution_.Δz * sf.boundary_.bz₁ - 1.0) *
                              sf(i, j, sf.size_.z - 1) +
                          resolution_.Δz * sf.boundary_.cz₁);

    return sf(i, j, k - 1) - 2.0 * sf(i, j, k) + sf(i, j, k + 1);
  }
};  // class Solver3D

namespace adi_solver {
enum class Direction { kX, kY, kZ };

// Alternating Direction Implicit solver for reaction diffusion equations
// class ADISolver3D : public Solver3D<mesh::Hexahedral> {
template <typename T, size_t reaction_count, size_t source_count,
          size_t auxiliary_reaction_count, size_t auxiliary_source_count,
          typename... Fields>
class ADISolver3D
    : public Solver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                      auxiliary_source_count, Fields...> {
 public:
  // constructors
  ADISolver3D() = default;  // default constructor
  __host__ __device__ ADISolver3D(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const MeshResolution<T>& mr, const T Δt = 0.0)
      : Solver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                 auxiliary_source_count, Fields...>(sf, mr, Δt),
        νx_{sf.diffusivity_ * Δt / (mr.Δx * mr.Δx)},
        νy_{sf.diffusivity_ * Δt / (mr.Δy * mr.Δy)},
        νz_{sf.diffusivity_ * Δt / (mr.Δz * mr.Δz)},
        intermediate_(sf.size_.xyz) {
    Initialize(sf);
  }                                                   // explicit constructor
  ADISolver3D(const ADISolver3D& adi) = default;      // copy constructor
  ADISolver3D(ADISolver3D&& adi) noexcept = default;  // move constructor

  // assignment operators
  ADISolver3D& operator=(const ADISolver3D& adi) = default;  // copy assignment
  ADISolver3D& operator=(ADISolver3D&& adi) noexcept =
      default;  // move assignment

  ~ADISolver3D() = default;  // default destructor

  __host__ __device__ inline void Initialize(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf)
      override {
    Initialize<Direction::kX>(sf);
    Initialize<Direction::kY>(sf);
    Initialize<Direction::kZ>(sf);
  }

  __host__ __device__ void TimeStep(
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>& sf,
      Fields&... fs) override {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of "
                  "reactions required\n");

    // Advance the system by one time step
    // When diffusion is restricted along x-direction
    if (sf.size_.x == 1)
      MarchXDiffusionRestricted(sf, fs...);
    else {
      Vector<T> diagonal_x(sf.size_.x), superdiagonal_x(sf.size_.x - 1),
          constant_x(sf.size_.x);
      for (size_t z_index = 0; z_index < sf.size_.z; z_index++)
        for (size_t y_index = 0; y_index < sf.size_.y; y_index++)
          MarchXSingleSystem(y_index, z_index, diagonal_x, superdiagonal_x,
                             constant_x, sf, fs...);
    }

    // When diffusion is not restricted along y-direction
    if (sf.size_.y not_eq 1) {
      Vector<T> diagonal_y(sf.size_.y), superdiagonal_y(sf.size_.y - 1),
          constant_y(sf.size_.y);
      for (size_t x_index = 0; x_index < sf.size_.x; x_index++)
        for (size_t z_index = 0; z_index < sf.size_.z; z_index++)
          MarchYSingleSystem(z_index, x_index, diagonal_y, superdiagonal_y,
                             constant_y, sf, fs...);
    }

    // When diffusion is restricted along z-direction
    if (sf.size_.z == 1)
      MarchZDiffusionRestricted(sf, fs...);
    else {
      Vector<T> diagonal_z(sf.size_.z), superdiagonal_z(sf.size_.z - 1),
          constant_z(sf.size_.z);
      for (size_t y_index = 0; y_index < sf.size_.y; y_index++)
        for (size_t x_index = 0; x_index < sf.size_.x; x_index++)
          MarchZSingleSystem(x_index, y_index, diagonal_z, superdiagonal_z,
                             constant_z, sf, fs...);
    }
  }

  // (1 - Ax/2)fⁿ* = (1 + Ax/2 + Ay + Az)fⁿ + ΔtHⁿ
  // Constant term: (1 + Ax/2 + Ay + Az)fⁿ + ΔtHⁿ
  __host__ __device__ void MarchXSingleSystem(
      const size_t y_index, const size_t z_index, Vector<T>& diagonal_x,
      Vector<T>& superdiagonal_x, Vector<T>& constant_x,
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>& sf,
      Fields&... fs) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of "
                  "reactions required\n");

    // Create TriDiagonal Matrix
    // Left boundary
    diagonal_x[0] = diagonal₀_x_;
    superdiagonal_x[0] = superdiagonal₀_x_;

    constant_x[0] = sf(0, y_index, z_index) +
                    OperationAx(sf, 0, y_index, z_index) / 2.0 +
                    OperationAy(sf, 0, y_index, z_index) +
                    OperationAz(sf, 0, y_index, z_index) -
                    νx_ * this->resolution_.Δx * sf.boundary_.cx₀;
    // reaction
    if constexpr (reaction_count)
      constant_x[0] +=
          this->Δt_ * HeterogeneityReaction(0, y_index, z_index, sf, fs...);
    // source
    if constexpr (source_count)
      constant_x[0] += this->Δt_ * HeterogeneitySource(0, y_index, z_index, sf);

    // Open interval
    for (size_t i = 1; i < sf.size_.x - 1; i++) {
      // values are same for 1 ≤ i ≤ sf.size_.x - 2
      diagonal_x[i] = diagonal_i_x_;
      superdiagonal_x[i] = superdiagonal_i_x_;

      constant_x[i] = sf(i, y_index, z_index) +
                      OperationAx(sf, i, y_index, z_index) / 2.0 +
                      OperationAy(sf, i, y_index, z_index) +
                      OperationAz(sf, i, y_index, z_index);
      // reaction
      if constexpr (reaction_count)
        constant_x[i] +=
            this->Δt_ * HeterogeneityReaction(i, y_index, z_index, sf, fs...);
      // source
      if constexpr (source_count)
        constant_x[i] +=
            this->Δt_ * HeterogeneitySource(i, y_index, z_index, sf);
    }

    // Right boundary
    diagonal_x[sf.size_.x - 1] = diagonal_end_x_;

    constant_x[sf.size_.x - 1] =
        sf(sf.size_.x - 1, y_index, z_index) +
        OperationAx(sf, sf.size_.x - 1, y_index, z_index) / 2.0 +
        OperationAy(sf, sf.size_.x - 1, y_index, z_index) +
        OperationAz(sf, sf.size_.x - 1, y_index, z_index) +
        νx_ * this->resolution_.Δx * sf.boundary_.cx₁;
    // reaction
    if constexpr (reaction_count)
      constant_x[sf.size_.x - 1] +=
          this->Δt_ *
          HeterogeneityReaction(sf.size_.x - 1, y_index, z_index, sf, fs...);
    // source
    if constexpr (source_count)
      constant_x[sf.size_.x - 1] +=
          this->Δt_ * HeterogeneitySource(sf.size_.x - 1, y_index, z_index, sf);

    // solve in place  for intermediate
    SolveSystem<Direction::kX>(diagonal_x, superdiagonal_x, constant_x);
    for (size_t i = 0; i < sf.size_.x; i++)
      intermediate_[sf.CartesianToLinearIndex(i, y_index, z_index)] =
          constant_x[i];
  }

  __host__ __device__ void MarchXDiffusionRestricted(
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>& sf,
      Fields&... fs) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of "
                  "reactions required\n");

    for (size_t k = 0; k < sf.size_.z; k++)
      for (size_t j = 0; j < sf.size_.y; j++) {
        intermediate_[sf.CartesianToLinearIndex(0, j, k)] =
            sf(0, j, k) + OperationAy(sf, 0, j, k) + OperationAz(sf, 0, j, k);

        // reaction
        if constexpr (reaction_count)
          intermediate_[sf.CartesianToLinearIndex(0, j, k)] +=
              this->Δt_ * HeterogeneityReaction(0, j, k, sf, fs...);
        // source
        if constexpr (source_count)
          intermediate_[sf.CartesianToLinearIndex(0, j, k)] +=
              this->Δt_ * HeterogeneitySource(0, j, k, sf);
      }
  }

  // (1 - Ay/2)fⁿ** = fⁿ* - Ay/2fⁿ
  // Costant term: fⁿ* - Ay/2fⁿ
  __host__ __device__ void MarchYSingleSystem(
      const size_t z_index, const size_t x_index, Vector<T>& diagonal_y,
      Vector<T>& superdiagonal_y, Vector<T>& constant_y,
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>& sf,
      Fields... /* fs */) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of "
                  "reactions required\n");

    // Create TriDiagonal Matrix and update constant vector
    // Left boundary
    diagonal_y[0] = diagonal₀_y_;
    superdiagonal_y[0] = superdiagonal₀_y_;

    constant_y[0] =
        intermediate_[sf.CartesianToLinearIndex(x_index, 0, z_index)] -
        OperationAy(sf, x_index, 0, z_index) / 2.0 -
        νy_ * this->resolution_.Δy * sf.boundary_.cy₀;

    // Open interval
    for (size_t j = 1; j < sf.size_.y - 1; j++) {
      // values are same for 1 ≤ i ≤ sf.size_.y - 2
      diagonal_y[j] = diagonal_j_y_;
      superdiagonal_y[j] = superdiagonal_j_y_;

      constant_y[j] =
          intermediate_[sf.CartesianToLinearIndex(x_index, j, z_index)] -
          OperationAy(sf, x_index, j, z_index) / 2.0;
    }

    // Right boundary
    diagonal_y[sf.size_.y - 1] = diagonal_end_y_;

    constant_y[sf.size_.y - 1] =
        intermediate_[sf.CartesianToLinearIndex(x_index, sf.size_.y - 1,
                                                z_index)] -
        OperationAy(sf, x_index, sf.size_.y - 1, z_index) / 2.0 +
        νy_ * this->resolution_.Δy * sf.boundary_.cy₁;

    // solve in place  for intermediate
    SolveSystem<Direction::kY>(diagonal_y, superdiagonal_y, constant_y);
    for (size_t j = 0; j < sf.size_.y; j++)
      intermediate_[sf.CartesianToLinearIndex(x_index, j, z_index)] =
          constant_y[j];
  }

  // (1 - Az/2)fⁿ⁺¹ = fⁿ** - Az/2fⁿ + Δt/2(Hⁿ⁺¹ - Hⁿ)
  // Constant term: fⁿ** - Az/2fⁿ - Δt/2 Hⁿ
  // Constant residual term: Constant(Δt/2 Hⁿ⁺¹)
  // Variable residual term: -Variable(Δt/2 Hⁿ⁺¹)
  __host__ __device__ void MarchZSingleSystem(
      const size_t x_index, const size_t y_index, Vector<T>& diagonal_z,
      Vector<T>& superdiagonal_z, Vector<T>& constant_z,
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>& sf,
      Fields... fs) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of "
                  "reactions required\n");

    // TODO: do the pack expansion
    // Create TriDiagonal Matrix and update constant vector
    // Left boundary
    diagonal_z[0] = diagonal₀_z_;
    superdiagonal_z[0] = superdiagonal₀_z_;

    constant_z[0] =
        intermediate_[sf.CartesianToLinearIndex(x_index, y_index, 0)] -
        OperationAz(sf, x_index, y_index, 0) / 2.0 -
        νz_ * this->resolution_.Δz * sf.boundary_.cz₀;
    // reaction
    if constexpr (reaction_count) {
      diagonal_z[0] -=
          this->Δt_ / 2.0 *
          HeterogeneityReactionResidualVariable(x_index, y_index, 0, sf, fs...);
      constant_z[0] += this->Δt_ / 2.0 *
                       (HeterogeneityReactionResidualConstant(x_index, y_index,
                                                              0, sf, fs...) -
                        HeterogeneityReaction(x_index, y_index, 0, sf, fs...));
    }
    // source
    if constexpr (source_count) {
      diagonal_z[0] -=
          this->Δt_ / 2.0 *
          HeterogeneitySourceResidualVariable(x_index, y_index, 0, sf);
      constant_z[0] +=
          this->Δt_ / 2.0 *
          (HeterogeneitySourceResidualConstant(x_index, y_index, 0, sf) -
           HeterogeneitySource(x_index, y_index, 0, sf));
    }

    // Open interval
    for (size_t k = 1; k < sf.size_.z - 1; k++) {
      // values are same for 1 ≤ i ≤ sf.size_.z - 2
      diagonal_z[k] = diagonal_k_z_;
      superdiagonal_z[k] = superdiagonal_k_z_;

      constant_z[k] =
          intermediate_[sf.CartesianToLinearIndex(x_index, y_index, k)] -
          OperationAz(sf, x_index, y_index, k) / 2.0;
      // reaction
      if constexpr (reaction_count) {
        diagonal_z[k] -= this->Δt_ / 2.0 *
                         HeterogeneityReactionResidualVariable(x_index, y_index,
                                                               k, sf, fs...);
        constant_z[k] +=
            this->Δt_ / 2.0 *
            (HeterogeneityReactionResidualConstant(x_index, y_index, k, sf,
                                                   fs...) -
             HeterogeneityReaction(x_index, y_index, k, sf, fs...));
      }
      // source
      if constexpr (source_count) {
        diagonal_z[k] -=
            this->Δt_ / 2.0 *
            HeterogeneitySourceResidualVariable(x_index, y_index, k, sf);
        constant_z[k] +=
            this->Δt_ / 2.0 *
            (HeterogeneitySourceResidualConstant(x_index, y_index, k, sf) -
             HeterogeneitySource(x_index, y_index, k, sf));
      }
    }

    // Right boundary
    diagonal_z[sf.size_.z - 1] = diagonal_end_z_;

    constant_z[sf.size_.z - 1] =
        intermediate_[sf.CartesianToLinearIndex(x_index, y_index,
                                                sf.size_.z - 1)] -
        OperationAz(sf, x_index, y_index, sf.size_.z - 1) / 2.0 +
        νz_ * this->resolution_.Δz * sf.boundary_.cz₁;

    // reaction
    if constexpr (reaction_count) {
      diagonal_z[sf.size_.z - 1] -=
          this->Δt_ / 2.0 *
          HeterogeneityReactionResidualVariable(x_index, y_index,
                                                sf.size_.z - 1, sf, fs...);
      constant_z[sf.size_.z - 1] +=
          this->Δt_ / 2.0 *
          (HeterogeneityReactionResidualConstant(x_index, y_index,
                                                 sf.size_.z - 1, sf, fs...) -
           HeterogeneityReaction(x_index, y_index, sf.size_.z - 1, sf, fs...));
    }
    // source
    if constexpr (source_count) {
      diagonal_z[sf.size_.z - 1] -= this->Δt_ / 2.0 *
                                    HeterogeneitySourceResidualVariable(
                                        x_index, y_index, sf.size_.z - 1, sf);
      constant_z[sf.size_.z - 1] +=
          this->Δt_ / 2.0 *
          (HeterogeneitySourceResidualConstant(x_index, y_index, sf.size_.z - 1,
                                               sf) -
           HeterogeneitySource(x_index, y_index, sf.size_.z - 1, sf));
    }

    // solve in place  for intermediate
    SolveSystem<Direction::kZ>(diagonal_z, superdiagonal_z, constant_z);
    for (size_t k = 0; k < sf.size_.z; k++)
      sf(x_index, y_index, k) = constant_z[k];
  }

  __host__ __device__ void MarchZDiffusionRestricted(
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>& sf,
      Fields&... fs) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of "
                  "reactions required\n");

    for (size_t j = 0; j < sf.size_.y; j++)
      for (size_t i = 0; i < sf.size_.x; i++) {
        sf(i, j, 0) = intermediate_[sf.CartesianToLinearIndex(i, j, 0)];

        // reaction
        if constexpr (reaction_count)
          sf(i, j, 0) +=
              this->Δt_ / 2.0 *
              (HeterogeneityReactionResidualConstant(i, j, 0, sf, fs...) -
               HeterogeneityReaction(i, j, 0, sf, fs...));
        // source
        if constexpr (source_count)
          sf(i, j, 0) += this->Δt_ / 2.0 *
                         (HeterogeneitySourceResidualConstant(i, j, 0, sf) -
                          HeterogeneitySource(i, j, 0, sf));
      }
  }

  __host__ friend void ADISolver3DSetup(
      ADISolver3D** adi,
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>* const* sf,
      const MeshResolution<T>& mr, const T Δt = 0.0) {
    ADISolver3DSetupKernel<<<1, 1>>>(adi, sf, mr, Δt);
  }

  __host__ friend void ADISolver3DUpdate(
      ADISolver3D** adi,
      ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count>* const* sf,
      const MeshResolution<T>& mr, const T Δt = 0.0) {
    ADISolver3DUpdateKernel<<<1, 1>>>(adi, sf, mr, Δt);
  }

 protected:
  /* data */

 private:
  /* data */
  // TODO: Think about memory coalescing for intermediate
  T νx_{}, νy_{}, νz_{};
  T subdiagonal_i_x_{}, subdiagonal_end_x_{}, subdiagonal_j_y_{},
      subdiagonal_end_y_{}, subdiagonal_k_z_{}, subdiagonal_end_z_{};
  T diagonal₀_x_{}, diagonal_i_x_{}, diagonal_end_x_{}, diagonal₀_y_{},
      diagonal_j_y_{}, diagonal_end_y_{}, diagonal₀_z_{}, diagonal_k_z_{},
      diagonal_end_z_{};
  T superdiagonal₀_x_{}, superdiagonal_i_x_{}, superdiagonal₀_y_{},
      superdiagonal_j_y_{}, superdiagonal₀_z_{}, superdiagonal_k_z_{};
  Vector<T> intermediate_{};

  /* methods */
  // TODO: Template version
  // Ax = νx * δx²
  __host__ __device__ inline T OperationAx(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const size_t i, const size_t j, const size_t k) {
    return νx_ * this->Operationδ²x(sf, i, j, k);
  }
  // Ay = νy * δy²
  __host__ __device__ inline T OperationAy(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const size_t i, const size_t j, const size_t k) {
    return νy_ * this->Operationδ²y(sf, i, j, k);
  }
  // Az = νz * δz²
  __host__ __device__ inline T OperationAz(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const size_t i, const size_t j, const size_t k) {
    return νz_ * this->Operationδ²z(sf, i, j, k);
  }

  template <Direction direction>
  __host__ __device__ void Initialize(
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf) {
    // dimension of tridiagonal system of equations for the direction
    size_t dimension{};
    if constexpr (direction == Direction::kX)
      dimension = sf.size_.x;
    else if (direction == Direction::kY)
      dimension = sf.size_.y;
    else  // Direction::kZ
      dimension = sf.size_.z;

    // return When diffusion is restricted along the -direction
    if (dimension == 1) return;

    // set direction specific variables at compile time
    T ν{};
    T b₀{}, c₀{}, b₁{}, c₁{};
    T resolution{};
    T *l_i{}, *l_end{}, *d₀{}, *d_i{}, *d_end{}, *u₀{}, *u_i{};
    if constexpr (direction == Direction::kX) {
      ν = νx_;
      b₀ = sf.boundary_.bx₀, c₀ = sf.boundary_.cx₀;  // left boundary
      b₁ = sf.boundary_.bx₁, c₁ = sf.boundary_.cx₁;  // right boundary
      resolution = this->resolution_.Δx;
      l_i = &subdiagonal_i_x_, l_end = &subdiagonal_end_x_;
      d₀ = &diagonal₀_x_, d_i = &diagonal_i_x_, d_end = &diagonal_end_x_;
      u₀ = &superdiagonal₀_x_, u_i = &superdiagonal_i_x_;
    } else if (direction == Direction::kY) {
      ν = νy_;
      b₀ = sf.boundary_.by₀, c₀ = sf.boundary_.cy₀;  // left boundary
      b₁ = sf.boundary_.by₁, c₁ = sf.boundary_.cy₁;  // right boundary
      resolution = this->resolution_.Δy;
      l_i = &subdiagonal_j_y_, l_end = &subdiagonal_end_y_;
      d₀ = &diagonal₀_y_, d_i = &diagonal_j_y_, d_end = &diagonal_end_y_;
      u₀ = &superdiagonal₀_y_, u_i = &superdiagonal_j_y_;
    } else {  // Direction::kZ
      ν = νz_;
      b₀ = sf.boundary_.bz₀, c₀ = sf.boundary_.cz₀;  // left boundary
      b₁ = sf.boundary_.bz₁, c₁ = sf.boundary_.cz₁;  // right boundary
      resolution = this->resolution_.Δz;
      l_i = &subdiagonal_k_z_, l_end = &subdiagonal_end_z_;
      d₀ = &diagonal₀_z_, d_i = &diagonal_k_z_, d_end = &diagonal_end_z_;
      u₀ = &superdiagonal₀_z_, u_i = &superdiagonal_k_z_;
    }

    // Create sub (l), main (d), and super diagonals (u) of tridiagonal matrix
    // values are same for 0 ≤ i ≤ dimension - 3
    *l_i = *l_end = -ν / 2.0;
    // values are same for 1 ≤ i ≤ dimension - 2
    *d_i = 1.0 + ν;
    // values are same for 1 ≤ i ≤ dimension - 2
    *u₀ = *u_i = -ν / 2.0;
    // Refine left boundary
    if (b₀ == 0 && c₀ == 0)  // no flux
      *d₀ = 1.0 + ν / 2.0;
    else {
      *d₀ = 1.0 - ν * (resolution * b₀ - 1.0);
      *u₀ = -ν;
    }
    // Refine right boundary
    if (b₁ == 0 && c₁ == 0)  //  no flux
      *d_end = 1.0 + ν / 2.0;
    else {
      *l_end = -ν;
      *d_end = 1.0 - ν * (-resolution * b₁ - 1.0);
    }
  }

  // location, function pointer, and scalar field inputs to the function
  __host__ __device__ inline T HeterogeneityReaction(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const Fields&... fs) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of reactions "
                  "required\n");
    T heterogeneity[sizeof...(Fields)] = {
        sf.HeterogeneityReaction(i, j, k, fs)...};
    return Accumulate(heterogeneity, heterogeneity + sizeof...(Fields), (T)0);
  }

  __host__ __device__ inline T HeterogeneityReactionResidualConstant(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const Fields&... fs) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of reactions "
                  "required\n");
    T heterogeneity[sizeof...(Fields)] = {
        sf.HeterogeneityReactionResidualConstant(i, j, k, fs)...};
    return Accumulate(heterogeneity, heterogeneity + sizeof...(Fields), (T)0);
  }

  __host__ __device__ inline T HeterogeneityReactionResidualVariable(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf,
      const Fields&... fs) {
    static_assert(reaction_count == sizeof...(Fields),
                  "mismatch between input number and the number of reactions "
                  "required\n");
    T heterogeneity[sizeof...(Fields)] = {
        sf.HeterogeneityReactionResidualVariable(i, j, k, fs)...};
    return Accumulate(heterogeneity, heterogeneity + sizeof...(Fields), (T)0);
  }

  template <typename U>
  __host__ __device__ inline U Accumulate(U* begin, U* end,
                                          U init_value = (U)0) {
    for (; begin not_eq end; begin++) init_value += *begin;
    return init_value;
  }

  // location, function pointer, and scalar field inputs to the function
  __host__ __device__ inline T HeterogeneitySource(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf) {
    return sf.HeterogeneitySource(i, j, k);
  }

  __host__ __device__ inline T HeterogeneitySourceResidualConstant(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf) {
    return sf.HeterogeneitySourceResidualConstant(i, j, k);
  }

  __host__ __device__ inline T HeterogeneitySourceResidualVariable(
      const size_t i, const size_t j, const size_t k,
      const ScalarField<T, reaction_count, source_count,
                        auxiliary_reaction_count, auxiliary_source_count>& sf) {
    return sf.HeterogeneitySourceResidualVariable(i, j, k);
  }

  template <Direction direction>
  __host__ __device__ void SolveSystem(Vector<T>& diagonal,
                                       Vector<T>& superdiagonal,
                                       Vector<T>& constant) {
    size_t N = diagonal.Size();
    T subdiagonal_i{}, subdiagonal_end{};
    if constexpr (direction == Direction::kX) {
      subdiagonal_i = subdiagonal_i_x_;
      subdiagonal_end = subdiagonal_end_x_;
    } else if (direction == Direction::kY) {
      subdiagonal_i = subdiagonal_j_y_;
      subdiagonal_end = subdiagonal_end_y_;
    } else  // Direction::kZ
    {
      subdiagonal_i = subdiagonal_k_z_;
      subdiagonal_end = subdiagonal_end_z_;
    }

    // Forward sweep
    constant[0] /= diagonal[0];
    superdiagonal[0] /= diagonal[0];
    // Note: subdiagonal_i  = subdiagonal[i-1] i ≥ 1, ..., n-1
    T denominator{};
    for (size_t index = 1; index < (N - 1); index++) {
      denominator = diagonal[index] - subdiagonal_i * superdiagonal[index - 1];
      superdiagonal[index] /= denominator;
      constant[index] -= subdiagonal_i * constant[index - 1];
      constant[index] /= denominator;
    }
    constant[N - 1] -= subdiagonal_end * constant[N - 2];
    constant[N - 1] /= diagonal[N - 1] - subdiagonal_end * superdiagonal[N - 2];

    // Back substituition
    for (int index = N - 2; index >= 0; index--)
      constant[index] -= superdiagonal[index] * constant[index + 1];
  }
};  // class ADISolver3D

// setup CUDA kernels
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0, typename... Fields>
__global__ void ADISolver3DSetupKernel(
    ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count, Fields...>** adi,
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>* const* sf,
    const MeshResolution<T> d_mr, const T Δt = 0.0) {
  (*adi) =
      new ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                      auxiliary_source_count, Fields...>(**sf, d_mr, Δt);
}

template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0, typename... Fields>
__global__ void ADISolver3DUpdateKernel(
    ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count, Fields...>** adi,
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>* const* sf,
    const MeshResolution<T> d_mr, const T Δt = 0.0) {
  (**adi) =
      ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                  auxiliary_source_count, Fields...>(**sf, d_mr, Δt);
}

template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0, typename... Fields>
__global__ void ADISolver3DDeleteKernel(
    ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count, Fields...>** adi) {
  delete (*adi);
}

// Time step CUDA kernels
template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0, typename... Fields>
void ADISolver3DTimeStep(
    const size_t size_x, const size_t size_y, const size_t size_z,
    ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count, Fields...>** adi,
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** sf,
    Fields**... fs) {
  constexpr size_t warpsize = 32;
  constexpr size_t n_parallel_warps = 1;
  constexpr size_t numSMs = 13;  // K80
  size_t blockSize, numBlocks;

  // Advance the system by one time step
  if (size_x == 1)
    ADISolver3DMarchXKernel<<<1, 1>>>(adi, sf, fs...);
  else {
    blockSize = (size_y > n_parallel_warps * warpsize)
                    ? n_parallel_warps * warpsize
                    : (size_y + warpsize - 1) / warpsize;
    numBlocks = (size_z + blockSize - 1) / blockSize > 2 * numSMs
                    ? 2 * numSMs
                    : (size_z + blockSize - 1) / blockSize;
    ADISolver3DMarchXKernel<<<numBlocks, blockSize>>>(adi, sf, fs...);
    // ADISolver3DMarchXKernel<<<1, 1>>>(adi, sf, fs...);
  }

  // When diffusion is not restricted along y-direction
  if (size_y not_eq 1) {
    blockSize = (size_y > n_parallel_warps * warpsize)
                    ? n_parallel_warps * warpsize
                    : (size_z + warpsize - 1) / warpsize;
    numBlocks = (size_x + blockSize - 1) / blockSize > 2 * numSMs
                    ? 2 * numSMs
                    : (size_x + blockSize - 1) / blockSize;
    ADISolver3DMarchYKernel<<<numBlocks, blockSize>>>(adi, sf, fs...);
    // ADISolver3DMarchYKernel<<<1, 1>>>(adi, sf, fs...);
  }

  // When diffusion is restricted along z-direction
  if (size_z == 1)
    ADISolver3DMarchZKernel<<<1, 1>>>(adi, sf, fs...);
  else {
    blockSize = (size_y > n_parallel_warps * warpsize)
                    ? n_parallel_warps * warpsize
                    : (size_x + warpsize - 1) / warpsize;
    numBlocks = (size_y + blockSize - 1) / blockSize > 2 * numSMs
                    ? 2 * numSMs
                    : (size_y + blockSize - 1) / blockSize;
    ADISolver3DMarchZKernel<<<numBlocks, blockSize>>>(adi, sf, fs...);
    //  ADISolver3DMarchZKernel<<<1, 1>>>(adi, sf, fs...);
  }
}

template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0, typename... Fields>
__global__ void ADISolver3DMarchXKernel(
    ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count, Fields...>** adi,
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** sf,
    Fields**... fs) {
  // When diffusion is restricted along x-direction
  if ((*sf)->size_.x == 1) {
    (*adi)->MarchXDiffusionRestricted(**sf, **fs...);
    return;
  }

  Vector<T> diagonal_x((*sf)->size_.x), superdiagonal_x((*sf)->size_.x - 1),
      constant_x((*sf)->size_.x);
  size_t y_index, z_index;
  for (size_t i = blockIdx.x * blockDim.x + threadIdx.x; i < (*sf)->size_.yz;
       i += blockDim.x * gridDim.x) {
    z_index = i / (*sf)->size_.y;
    y_index = i % (*sf)->size_.y;

    (*adi)->MarchXSingleSystem(y_index, z_index, diagonal_x, superdiagonal_x,
                               constant_x, **sf, **fs...);
  }
}

template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0, typename... Fields>
__global__ void ADISolver3DMarchYKernel(
    ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count, Fields...>** adi,
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** sf,
    Fields**... fs) {
  // When diffusion is restricted along y-direction
  if ((*sf)->size_.y == 1) return;

  Vector<T> diagonal_y((*sf)->size_.y), superdiagonal_y((*sf)->size_.y - 1),
      constant_y((*sf)->size_.y);
  size_t z_index, x_index;
  for (size_t i = blockIdx.x * blockDim.x + threadIdx.x; i < (*sf)->size_.zx;
       i += blockDim.x * gridDim.x) {
    x_index = i / (*sf)->size_.z;
    z_index = i % (*sf)->size_.z;

    (*adi)->MarchYSingleSystem(z_index, x_index, diagonal_y, superdiagonal_y,
                               constant_y, **sf, **fs...);
  }
}

template <typename T = double, size_t reaction_count = 0,
          size_t source_count = 0, size_t auxiliary_reaction_count = 0,
          size_t auxiliary_source_count = 0, typename... Fields>
__global__ void ADISolver3DMarchZKernel(
    ADISolver3D<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count, Fields...>** adi,
    ScalarField<T, reaction_count, source_count, auxiliary_reaction_count,
                auxiliary_source_count>** sf,
    Fields**... fs) {
  // When diffusion is restricted along y-direction
  if ((*sf)->size_.z == 1) {
    (*adi)->MarchZDiffusionRestricted(**sf, **fs...);
    return;
  }

  Vector<T> diagonal_z((*sf)->size_.z), superdiagonal_z((*sf)->size_.z - 1),
      constant_z((*sf)->size_.z);
  size_t x_index, y_index;
  for (size_t i = blockIdx.x * blockDim.x + threadIdx.x; i < (*sf)->size_.xy;
       i += blockDim.x * gridDim.x) {
    y_index = i / (*sf)->size_.x;
    x_index = i % (*sf)->size_.x;

    (*adi)->MarchZSingleSystem(x_index, y_index, diagonal_z, superdiagonal_z,
                               constant_z, **sf, **fs...);
  }
}
}  // namespace adi_solver
}  // namespace solvers
#endif  //	 SOLVERS_HPP_INCLUDED
