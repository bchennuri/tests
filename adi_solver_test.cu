// related header
#include "scalar_field.hpp"
#include "solvers.hpp"

// c standard library headers
#include <cstdio>

// c++ standard library headers
#include <iostream>
#include <numeric>
#include <vector>

// other library headers
#define BOOST_TEST_MODULE ADI Solver Test GPU
#include <boost/test/included/unit_test.hpp>  // for boost unit testing

// project headers

#define PRINT_FLAG true

BOOST_AUTO_TEST_CASE(Two_D_XY_Direction) {
  using namespace solvers;
  using namespace solvers::adi_solver;

  constexpr double Δx = 0.1, Δy = 0.1, Δz = 0.1;
  constexpr double D = 1.0, dt = 0.5e-6;
  // no-flux boundary conditions
  constexpr double bx₀ = 0.0, cx₀ = 0.0, bx₁ = 0.0, cx₁ = 0.0;
  constexpr double by₀ = 0.0, cy₀ = 0.0, by₁ = 0.0, cy₁ = 0.0;
  constexpr double bz₀ = 0.0, cz₀ = 0.0, bz₁ = 0.0, cz₁ = 0.0;
  constexpr size_t size_x = 1025, size_y = 1025, size_z = 1;
  // constexpr size_t size_x = 65, size_y = 65, size_z = 1;
  constexpr size_t count = 100;

  constexpr size_t reaction_count = 1;
  constexpr size_t buffer_source_count = 0;
  constexpr size_t ion_source_count = 0;

  constexpr double M = 1.0;

  // Set heap size for device
  size_t sizedd, HEAP_MEMORY = (size_t)1 << 32;  // ~4 GB
  GPUErrorCheck(cudaDeviceGetLimit(&sizedd, cudaLimitMallocHeapSize));
  GPUErrorCheck(cudaDeviceSetLimit(cudaLimitMallocHeapSize, HEAP_MEMORY));

  cudaSharedMemConfig config = cudaSharedMemBankSizeEightByte;
  GPUErrorCheck(cudaDeviceSetSharedMemConfig(config));

  Buffer<double, reaction_count, buffer_source_count, reaction_count,
         ion_source_count>
      h_test_buffer_symmetric(BoundaryConditions(bx₀, cx₀, bx₁, cx₁, by₀, cy₀,
                                                 by₁, cy₁, bz₀, cz₀, bz₁, cz₁),
                              MeshSize(size_x, size_y, size_z), D);
  // Device pointers, using upscaling
  ScalarField<double, reaction_count, buffer_source_count, reaction_count,
              ion_source_count> **d_test_buffer_symmetric;

  Ion<double, reaction_count, ion_source_count, reaction_count,
      buffer_source_count>
      h_test_ion_symmetric(BoundaryConditions(bx₀, cx₀, bx₁, cx₁, by₀, cy₀, by₁,
                                              cy₁, bz₀, cz₀, bz₁, cz₁),
                           MeshSize(size_x, size_y, size_z), D);
  // Device pointers, using upscaling
  ScalarField<double, reaction_count, ion_source_count, reaction_count,
              buffer_source_count> **d_test_ion_symmetric;

  // Allocate device memory
  GPUErrorCheck(cudaAlloc(d_test_buffer_symmetric, 1));
  GPUErrorCheck(cudaAlloc(d_test_ion_symmetric, 1));

  // Initialize
  h_test_ion_symmetric(size_x / 2, size_y / 2, 0) = M / (Δx * Δy);

  // copy from host to device
  CopyIonFromHostToDevice(d_test_ion_symmetric, h_test_ion_symmetric);

  // Solver for ion
  ADISolver3D<double, reaction_count, ion_source_count, reaction_count,
              buffer_source_count,
              ScalarField<double, reaction_count, buffer_source_count,
                          reaction_count, ion_source_count>> *
      *d_xy_solver_ion_symmetric;
  // Allocate device memory
  GPUErrorCheck(cudaAlloc(d_xy_solver_ion_symmetric, 1));

  ADISolver3DSetup(d_xy_solver_ion_symmetric, d_test_ion_symmetric,
                   MeshResolution(Δx, Δy, Δz), dt);

  // copy from host to device
  CopyBufferFromHostToDevice(d_test_buffer_symmetric, h_test_buffer_symmetric);

  // Solver for buffer
  ADISolver3D<double, reaction_count, buffer_source_count, reaction_count,
              ion_source_count,
              ScalarField<double, reaction_count, ion_source_count,
                          reaction_count, buffer_source_count>> *
      *d_xy_solver_buffer_symmetric;
  GPUErrorCheck(cudaAlloc(d_xy_solver_buffer_symmetric, 1));

  ADISolver3DSetup(d_xy_solver_buffer_symmetric, d_test_buffer_symmetric,
                   MeshResolution(Δx, Δy, Δz), dt / 2.0);

  // Staggered first time step
  ADISolver3DTimeStep(size_x, size_y, size_z, d_xy_solver_buffer_symmetric,
                      d_test_buffer_symmetric, d_test_ion_symmetric);

  ADISolver3DUpdate(d_xy_solver_buffer_symmetric, d_test_buffer_symmetric,
                    MeshResolution(Δx, Δy, Δz), dt / 2.0);

  for (size_t n = 0; n < count - 1; n++) {
    // time step ion
    ADISolver3DTimeStep(size_x, size_y, size_z, d_xy_solver_ion_symmetric,
                        d_test_ion_symmetric, d_test_buffer_symmetric);

    // time step buffer
    ADISolver3DTimeStep(size_x, size_y, size_z, d_xy_solver_buffer_symmetric,
                        d_test_buffer_symmetric, d_test_ion_symmetric);
  }
  // final time step ion
  ADISolver3DTimeStep(size_x, size_y, size_z, d_xy_solver_ion_symmetric,
                      d_test_ion_symmetric, d_test_buffer_symmetric);

  CopyScalarFromDeviceToHost(h_test_ion_symmetric.GetScalar().Data(),
                             d_test_ion_symmetric,
                             h_test_ion_symmetric.size_.xyz);

  BOOST_TEST(abs(h_test_ion_symmetric(size_x / 2, size_y / 2, 0) /
                     (M / (Δx * Δy) *
                      pow(erf(Δx / 2.0 / (2 * sqrt(D * count * dt))), 2.0)) -
                 1.0) < 0.05);

  double sum_symmetric =
      std::accumulate(
          h_test_ion_symmetric().Data(),
          h_test_ion_symmetric().Data() + h_test_ion_symmetric.size_.xyz, 0.0) *
      Δx * Δy;
  BOOST_TEST(abs(sum_symmetric / M - 1.0) < 1);
  if (PRINT_FLAG) {
    std::cout << "\nXY-Direction\nSum (symmetric): " << std::setprecision(10)
              << sum_symmetric << "\n";
    std::cout << "Value at source (simulated): " << std::setprecision(10)
              << h_test_ion_symmetric(size_x / 2, size_y / 2, 0) << "\n";
    std::cout << "Value at source (theoretical): " << std::setprecision(10)
              << (M / (Δx * Δy) *
                  pow(erf(Δx / 2.0 / (2 * sqrt(D * count * dt))), 2.0))
              << "\n";
    std::cout << "Value next to source (simulated): " << std::setprecision(10)
              << h_test_ion_symmetric(size_x / 2 + 1, size_y / 2, 0) << "\n";
  }

  GPUErrorCheck(cudaFree(d_test_buffer_symmetric));
  GPUErrorCheck(cudaFree(d_test_ion_symmetric));

  GPUErrorCheck(cudaFree(d_xy_solver_ion_symmetric));
  GPUErrorCheck(cudaFree(d_xy_solver_buffer_symmetric));
}
